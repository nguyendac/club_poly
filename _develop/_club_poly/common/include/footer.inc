<div class="ft_top">
  <div class="row">
    <div class="ft_top_left">
      <h1 class="ft_logo">
        <a href="/"><img src="/common/images/logo_bl.png" alt="Logo"></a>
      </h1>
      <!--/.logo-->
    </div>
    <!--/.ft_left-->
    <div class="ft_top_right">
      <div class="ft_contact">
        <a href="tel:0875747000">0875-74-7000</a>
        <em>営業時間：平日8:00～17:00</em>   
      </div>
      <!--/.contact-->
      <div class="ft_btn">
        <a href="tel:0875747000">お問い合せ・お見積もりフォーム</a> 
      </div>
      <!--/.ft_btn-->
    </div>
    <!--/.ft_right-->
  </div>
</div>
<div class="copyright">
  <div class="row">
    <p>&copy; 2018 CLUB POLY All Rights Reserved.</p>
  </div>
</div>

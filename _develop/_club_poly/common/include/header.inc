<div class="hd_top">
  <div class="row">
    <h1 class="hd_logo">
      <a href=""><img src="/common/images/logo.png" alt="Logo"></a>
    </h1>
    <!--/.logo-->
    <div class="hd_contact show_pc">
      <em>営業時間：平日8:00～17:00</em>
      <a href="tel:0875747000"><img src="/common/images/number.png" alt="Number"></a>
    </div>
    <!--/.contact-->
    <div class="menu show_sp" id="icon_nav">
      <div class="icon_menu">
        <div class="icon_inner"></div>
      </div>
    </div>
    <!--/.icon_nav-->
  </div>
</div>
<div class="hd_bottom">
  <div class="row">
    <nav class="nav" id="nav">
      <ul>
        <li><a class="anchor" href="/">選ばれる理由</a></li>
        <li><a class="anchor" href="#price">料金</a></li>
        <li><a class="anchor" href="#voice">お客様の声</a></li>
        <li><a class="anchor" href="#guide">ご利用ガイド</a></li>
        <li><a class="anchor" href="#qa">よくあるご質問</a></li>
        <li><a class="anchor" href="#flow">ご注文の流れ</a></li>
        <li><a class="anchor" href="#">お問い合せ・お見積もり</a></li>
      </ul>
    </nav>
  </div>
</div>
<!--/.bottom-->
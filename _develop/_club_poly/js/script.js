window.addEventListener('DOMContentLoaded',function(){
  new Faq();
})

// Arcodion
var Faq = (function(){
	function Faq(){
		f = this;
		this._question = document.querySelectorAll(".btn_voice");
		this.answer;
		this._toggle = function(el) {
			f.answer = closest(el,'.voice_inner_child_top').nextElementSibling;
			f.answer.classList.toggle("open");
		}
		Array.prototype.forEach.call(f._question, function(el, i){
			el.addEventListener("click",function(e){
				e.preventDefault();
				f._toggle(el);
			});
		});
	}
	return Faq;
})()
window.addEventListener('DOMContentLoaded',function(){
  //new Formm();
})
// var Formm = (function(){
//   function Formm(){
//     var f = this;
//     //this.register = 'register';
//     this.tabReg = document.getElementById('register');
//     this.contentReg = document.getElementById('ctRegister');
//     this._handle = function(){
//       f.contentReg.classList.add('select');
//       console.log(f.contentReg);
//     }
//     f.tabReg.addEventListener('click', function(){
//       f._handle();
//     })
//     //f._handle();
//   }
//   return Formm;
// })()

$(function() {
    $('label').on('click', function() {
        $(this).parent().parent().children('li').children('label').removeClass('select');
        if ($(this).children('span').children('input').prop('checked')) {
            $(this).addClass('select');
        }
    });
    $('label').on('click', function() {
        $(this).parent().children('label').removeClass('select');
        $(this).addClass('select');
        //console.log($(this).parent().children('label'));
    });
    $("label").children("input").change(function() {
        $("label").each(function() {
            if ($(this).children("input").prop('checked')) {
                $(this).addClass('select');
            } else {
                $(this).removeClass('select');
            }
        });
    });
    $('.option_select label').on('click', function() {
        if ($(this).children("span").text() == "無し") {
            $(".option_select .wpcf7-list-item label input").prop('checked', false);
            $(this).children("input").prop('checked', true);
        } else {
            $('.option_select .wpcf7-list-item:first-of-type input').prop('checked', false);
        }
    });
    $(".option_select label").children("input").change(function() {
        $(".option_select label").each(function() {
            if ($(this).children("input").prop('checked')) {
                $(this).addClass('select');
            } else {
                $(this).removeClass('select');
            }
            if ($(".tape .option_select label").filter(":last").hasClass("select")) {
                $(".tape .option_select .etc_text").show("slow");
            } else {
                $(".tape .option_select .etc_text").hide("slow");
            }
            if ($(".register .option_select label").filter(":last").hasClass("select")) {
                $(".register .option_select .etc_text").show("slow");
            } else {
                $(".register .option_select .etc_text").hide("slow");
            }
            if ($(".oval .option_select label").filter(":last").hasClass("select")) {
                $(".oval .option_select .etc_text").show("slow");
            } else {
                $(".oval .option_select .etc_text").hide("slow");
            }
            if ($(".fasion .option_select label").filter(":last").hasClass("select")) {
                $(".fasion .option_select .etc_text").show("slow");
            } else {
                $(".fasion .option_select .etc_text").hide("slow");
            }
        });
    });
    $('.day_select label').on('click', function() {
        if ($(this).children("input[name='day_select'][value='納期日指定']").prop('checked')) {
            $('.text_box').show("slow");
        } else {
            $('.text_box').hide("slow");
        }
    });
    $('.form_select').children('li').on('click', function() {
        var idname = $(this).attr("id");
        $('.contact_form').css("display", "none");
        $('.' + idname).css("display", "block");
        if ($("label").children("input:checked").val()) {
            $("input:checked").parent("label").addClass("select");
        }
    });
    $(".oval .material_select label").on('click', function() {
        if ($(this).children("span").text() == "HDPE（シャリシャリ）") {
            $(".oval .color_select .wpcf7-list-item:nth-of-type(2)").hide("slow");
            $(".oval .planning_select .wpcf7-list-item:nth-of-type(-n+3)").show("slow");
            $(".oval .planning_select .wpcf7-list-item:nth-of-type(n + 4):not(:nth-of-type(n + 7))").hide("slow");
            $(".oval .color_select .wpcf7-list-item:nth-of-type(1)").show("slow");
            $(".oval .color_select .wpcf7-list-item:nth-of-type(2)").children("input").prop('checked', false);
            $(".oval .color_select .wpcf7-list-item:nth-of-type(2)").removeClass("select")
        } else {
            $(".oval .color_select .wpcf7-list-item:nth-of-type(2)").show("slow");
            $(".oval .color_select .wpcf7-list-item:nth-of-type(1)").hide("slow");
            $(".oval .planning_select .wpcf7-list-item:nth-of-type(n + 4):not(:nth-of-type(n + 7))").show("slow");
            $(".oval .planning_select .wpcf7-list-item:nth-of-type(-n+3)").hide("slow");
            $(".oval .color_select .wpcf7-list-item:nth-of-type(1)").children("input").prop('checked', false);
            $(".oval .color_select .wpcf7-list-item:nth-of-type(1)").removeClass("select")
        }
    });
    $("input:checked").parent("label").addClass("select");
    $(window).on("load", function() {
        var has = location.hash.replace("#", "");
        if (has == "") {
            has = "register";
        }
        $('.' + has).show();
        $('#' + has).children("label").addClass("select");
        $('#' + has).children("label").children("span").children("input").prop('checked', true);
        if ($('#' + has).children("label").children("span").children("input").prop('checked')) {
            $(window).scrollTop(0);
        }
        return false;
    });
    // $(".contact_form label").unwrap();
    // $(".contact_form label").unwrap();
    // $(".contact_form label").unwrap();
    $('.register .planning_select .wpcf7-list-item:last-of-type .wpcf7-list-item-label').append($('.register .kikaku_etc'));
    $('.tape .planning_select .wpcf7-list-item:last-of-type .wpcf7-list-item-label').append($('.tape .kikaku_etc'));
    $('.oval .planning_select .wpcf7-list-item:last-of-type .wpcf7-list-item-label').append($('.oval .kikaku_etc'));
    $('.oval .option_select .wpcf7-list-item:nth-of-type(2) .wpcf7-list-item-label').append($('.oval .op_etc'));
    $('.fasion .planning_select .wpcf7-list-item:last-of-type .wpcf7-list-item-label').append($('.fasion .kikaku_etc'));
    // $(".zip").attr('onKeyUp', 'AjaxZip3.zip2addr(this,\'\',\'address\',\'address\');');
});
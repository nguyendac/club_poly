<div class="form_group">
  <h3>素材</h3>
  <p class="material_select">
    [radio material use_label_element default:1 "HDPE（シャリシャリ）" "LLDPE（ツルツル）"]
  </p>
</div>
<div class="form_group">
  <h3>規格サイズ</h3>
  <p class="kikaku_img"><span class="koban"></span></p>
  <p class="planning_select">
    [radio planning use_label_element default:1 "A4　　高400mm×横250mm-素材の厚み0.06mm" "B4　　高450mm×横300mm-素材の厚み0.07mm"
    "A3　　高500mm×横350mm-素材の厚み0.08mm"
    "A4　　高400mm×横250mm-厚み0.035mm"
    "B4　　高450mm×横300mm-厚み0.04mm"
    "A3　　高500mm×横350mm-厚み0.05mm"
    "　その他　"]
    <em class="kikaku_etc" id="kikaku_etc">高
      <span class="free_height">
        [text free_height]
      </span>mm×<br class="show_sp">横
      <span class="free_width">
        [text free_width]
      </span>mm×<br class="show_sp">マチ
      <span class="free_machi">
        [text free_machi]
      </span>mm-<br class="show_sp">素材の厚み
      <span class="free_atumi">
        [text free_atumi]
      </span>mm
    </em>
  </p>
</div>
<div class="form_group">
  <h3>生地色</h3>
  <p class="color_select three_select">
    [radio color use_label_element default:1 "透明" "半透明"
    "白" "黒" "茶"]
  </p>
</div>
<div class="form_group">
  <h3>印刷色数</h3>
  <p class="color_num_select three_select">
    [radio color_num use_label_element default:1 "1色" "2色"
    "4色" "1色ベタ塗り" "2色ベタ塗り"]
  </p>
</div>
<div class="form_group">
  <h3>印刷方法</h3>
  <p class="print_select three_select">
    [radio print use_label_element default:1 "片面印刷" "両面印刷（両面同柄）" "両面印刷（両面別柄）"]
  </p>
</div>
<div class="form_group">
  <h3>印刷部数</h3>
  <p class="print_num_select three_select">
    [radio print_num use_label_element default:1 "100枚" "500枚" "1,000枚" "2,000枚" "3,000枚" "5,000枚" "10,000枚" "20,000枚"]
  </p>
</div>
<div class="form_group">
  <h3>納期</h3>
  <p class="day_select two_select">
    [radio day_select use_label_element default:1 "納期日指定" "未定"]
  </p>
  <p class="day_select text_box">
    [text day_text placeholder "希望納品日がございまいたらご入力下さい"]
  </p>
</div>
<div class="form_group">
  <h3>その他オプション</h3>
  <p class="kikaku_img op"><span class="koban"></span></p>
  <p class="option_select">
    [checkbox option use_label_element "無し" "横マチ" "その他オプションのご希望"]
    <em class="op_etc">の寸法[text op_width]mm
    </em>
    <br>
    [textarea etc_box 40x10 class:etc_text]
  </p>
</div>
<div class="form_group">
  <h3>その他のご要望</h3>
  <p class="etc_select">
    [textarea textarea-683 40x10 class:etc_text placeholder "下記お客様ご住所と納品希望のご希望場所が異なる場合は納品場所をご入力ください。"]
  </p>
</div>
<div class="form_main">
  <h3>お客様情報</h3>
  <div class="form_group_contact">
    <label>会社名</label>
    <p>
      <span>
        [text your_company]
      </span>
    </p>
  </div>
  <div class="form_group_contact">
    <label>お名前</label>
    <p>
      <span>
        [text* your-name]
      </span>
    </p>
  </div>
  <div class="form_group_contact">
    <label>メールアドレス</label>
    <p>
      <span>
        [email* your-email]
      </span>
    </p>
  </div>
  <div class="form_group_contact">
    <label>電話番号</label>
    <p>
      <span>
        [tel your_tel]
      </span>
    </p>
  </div>
  <div class="form_group_contact">
    <label>郵便番号</label>
    <p>
      <span>
        [text zip class:zip]
      </span>
    </p>
  </div>
  <div class="form_group_contact">
    <label>住所</label>
    <p>
      <span>
        [text address]
      </span>
    </p>
  </div>
  <div class="form_group_contact">
    <label>その他要望</label>
    <p>
      <span>
        [textarea textarea-473 40x10]
      </span>
    </p>
  </div>
  <p class="btn">
    [submit class:submit_btn "上記内容で送信する"]
  </p>
</div>
<div class="form_group">
  <h3>規格サイズ</h3>
  <p class="kikaku_img"><span class="ktape"></span></p>
  <p class="planning_select">
    [radio planning use_label_element default:1 "A5　　高290mm×横200mm" "A5　　高290mm×横200mm" "A4　　高400mm×横250mm" "B4　　高450mm×横300mm" "その他　"]
    <em class="kikaku_etc">高[text free_height]mm×<br class="show_sp">横[text free_width]mm
    </em>
  </p>
</div>
<div class="form_group">
  <h3>厚み</h3>
  <p class="pl_select">
    [select* amtumi "0.1mm 薄めのクリアファイルのような厚みです" "0.08mm 通常のポリ袋の厚みです" "0.07mm 少し薄めポリ袋の厚みです" "0.06mm ファッション系ででよくご利用されるポリ袋の厚み" "0.04mm DMで使用される透明封筒の厚み"]
  </p>
</div>
<div class="form_group">
  <h3>素材</h3>
  <p class="material_select">
    [radio material use_label_element default:1 "HDPE（シャリシャリ）" "LLDPE（ツルツル）" "特注素材" "梨地"]
  </p>
</div>
<div class="form_group">
  <h3>生地色</h3>
  <p class="color_select three_select">
    [radio color use_label_element default:1 "ブルー" "オレンジ"
    "イエロー" "グリーン" "ピンク" "ブラウン" "モスグリーン" "アイボリー" "ホワイト" "ブラック" "シルバー"]
  </p>
</div>
<div class="form_group">
  <h3>印刷色数</h3>
  <p class="color_num_select three_select">
    [radio color_num use_label_element default:1 "1色" "2色"
    "4色" "1色ベタ塗り" "2色ベタ塗り"]
  </p>
</div>
<div class="form_group">
  <h3>印刷方法</h3>
   <p class="print_select three_select">
    [radio print use_label_element default:1 "片面印刷" "両面印刷（両面同柄）" "両面印刷（両面別柄）"]
  </p>
</div>
<div class="form_group">
  <h3>印刷部数</h3>
  <p class="print_num_select three_select">
    [radio print_num use_label_element default:1 "100枚" "500枚" "1,000枚" "2,000枚" "3,000枚" "5,000枚" "10,000枚" "20,000枚"]
  </p>
</div>
<div class="form_group">
  <h3>納期</h3>
  <p class="day_select two_select">
    [radio day_select use_label_element default:1 "納期日指定" "未定"]
  </p>
  <p class="day_select text_box">
    [text day_text placeholder "希望納品日がございまいたらご入力下さい"]
  </p>
</div>
<div class="form_group">
  <h3>その他オプション</h3>
  <p class="kikaku_img op"><span class="tape"></span></p>
  <p class="option_select">
    [checkbox option use_label_element "無し" "底マチ有り" "上部折り返し有り" "上部折り返し無し" "その他オプションのご希望"]
    <br>
    [textarea etc_box 40x10 class:etc_text]
  </p>
</div>
<div class="form_group">
  <h3>その他のご要望</h3>
  <p class="etc_select">
    [textarea textarea-683 40x10 class:etc_text placeholder "下記お客様ご住所と納品希望のご希望場所が異なる場合は納品場所をご入力ください。"]
  </p>
</div>
<div class="form_main">
  <h3>お客様情報</h3>
  <div class="form_group_contact">
    <label>会社名</label>
    <p>
      <span>
        [text your_company]
      </span>
    </p>
  </div>
  <div class="form_group_contact">
    <label>お名前</label>
    <p>
      <span>
        [text* your-name]
      </span>
    </p>
  </div>
  <div class="form_group_contact">
    <label>メールアドレス</label>
    <p>
      <span>
        [email* your-email]
      </span>
    </p>
  </div>
  <div class="form_group_contact">
    <label>電話番号</label>
    <p>
      <span>
        [tel your_tel]
      </span>
    </p>
  </div>
  <div class="form_group_contact">
    <label>郵便番号</label>
    <p>
      <span>
        [text zip class:zip]
      </span>
    </p>
  </div>
  <div class="form_group_contact">
    <label>住所</label>
    <p>
      <span>
        [text address]
      </span>
    </p>
  </div>
  <div class="form_group_contact">
    <label>その他要望</label>
    <p>
      <span>
        [textarea textarea-473 40x10]
      </span>
    </p>
  </div>
  <p class="btn">
    [submit class:submit_btn "上記内容で送信する"]
  </p>
</div>
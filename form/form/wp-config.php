<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'club_poly');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', '');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         's]_*sHxr=olYJScqu`h>7@:l&g<g%mYPUA~Rg%,y%II.nKgEQ ]zPZh6-rRLxV(:');
define('SECURE_AUTH_KEY',  '(%PL)KQ?RYJCGo?l8e_E0hrM[j*,=dpxh] #w$Cr_x8t)a$Sa;x[lzM@,LEVi&-x');
define('LOGGED_IN_KEY',    '` #AS#.St)B`YpVn@WaWsQ?07|yChVO1!LoT2NDVkR@Ysn&`7yG9&gmLSX0sP3Gx');
define('NONCE_KEY',        '6UE3}zG(la[Z?Qb]nz_3Q!U9ouJD@_>e2s5dZ||9bQ*j4M_>1S3fih_v5)Upx.oJ');
define('AUTH_SALT',        'fj@L/g!QFL0F;9_<hOXcg}}hhr*VA,$PCgp<x86xW arl)VIg@F6J[bl1u?Qu$gg');
define('SECURE_AUTH_SALT', 'NsKrhTu_gk$E}NnEWI)kg~a%_0lVNhY7=W# ]5f}=^qkM;$,JYZv_G6l6[`c-4oV');
define('LOGGED_IN_SALT',   'I2_Vr+7bs-a~5R!m{:B]v6CFSyAC+B#uj.CS;9=wAjeqN1HP{x+@ WM8`=3z6=O/');
define('NONCE_SALT',       '@Xl)^i-au50`zR2W|apKY_SH*CnEF,%/B>@6H[7j:gZ%0Ir!T?]17b!JVo)Nx N-');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');

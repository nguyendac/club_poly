<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="format-detection" content="telephone=no">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta name="Description" content="">
<meta name="Keywords" content="">
<title>お見積もりフォーム</title>

<meta property="og:title" content="">
<meta property="og:url" content="">
<meta property="og:image" content="/common/images/ogp.png">
<meta property="og:type" content="article">

<link rel="shortcut icon" href="/favicon.ico" type="image/x-icon">
<link rel="icon" href="/favicon.ico" type="image/x-icon">

<link href="https://fonts.googleapis.com/css?family=Montserrat" rel="stylesheet">
<link rel="stylesheet" href="/common/css/normalize.css?v=ccc10c60c0f52084eb4f60647bd2ad9e">
<link rel="stylesheet" href="/common/css/common.css?v=c734feb77be568d3a95a1a43b4557b1a">
<link rel="stylesheet" href="/asset_form/css/style.css?v=32424923e4dfb3f81d7eef6b677e31ce">
<?php wp_head();?>
</head>
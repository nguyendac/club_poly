<!DOCTYPE html>
<html lang="ja">

<?php get_header(); ?>

<body>
  <span class="load active" id="loading"><i></i></span>
  <div id="container" class="container">
    <header id="header" class="header">
      <?php get_template_part('template/template','header')?>
    </header>
    <main>
      <section class="form">
        <div class="ttl_section">
          <h2>お見積もりフォーム</h2>
        </div>
        <div class="breadcrum row">
          <ul class="breadcrum_list">
            <li><a href="/">トップページ</a></li>
            <li>お見積もりフォーム</li>
          </ul>
        </div>
        <div class="form_inner">
          <div class="row">
            <p class="form_inner_des">必要事項をご記入の上、「上記の内容で送信する」ボタンを押して下さい。専門スタッフがメールを確認後、即日対応させて頂きます。<br>（営業時間終了後の場合はご返信は翌営業日になります。ご了承ください）</p>
            <div class="form_inner_main">
               <h3>袋のかたちを選ぶ</h3>
               <ul class="form_select">
                  <li id="register" class="shape">
                     <label class="select">
                        <span><input type="radio" name="katachi" checked="checked">レジ袋</span>
                        <figure>
                          <img src="/asset_form/images/register.png?v=dab5e07118984ea5cdae77a1c32bbfdd" alt="レジ袋">
                        </figure>
                     </label>
                  </li>
                  <li id="oval" class="shape">
                     <label>
                        <span><input type="radio" name="katachi">小判抜き</span>
                        <figure>
                          <img src="/asset_form/images/oval.png?v=f0ff30d93ed5f0ae57a86ae020726677" alt="小判抜き">
                        </figure>
                     </label>
                  </li>
                  <li id="tape" class="shape">
                     <label>
                       <span><input type="radio" name="katachi">テープハンドル</span>
                       <figure>
                          <img src="/asset_form/images/tape.png?v=12a84d4df2999bfc754b28f1dd53060e" alt="テープハンドル">
                       </figure>
                     </label>
                  </li>
                  <li id="fasion" class="shape">
                     <label>
                        <span><input type="radio" name="katachi">ショルダーバッグ</span>
                        <figure>
                          <img src="/asset_form/images/fashion.png?v=040ea1bc5af50aa32393351957da310e" alt="ショルダーバッグ">
                        </figure>
                     </label>
                  </li>
               </ul>
               <div class="register contact_form">
                <?php echo do_shortcode('[contact-form-7 id="5" title="Register Form"]') ?>
               </div>
               <div class="oval contact_form">
                <?php echo do_shortcode('[contact-form-7 id="6" title="Oval Form"]') ?>
               </div>
               <div class="tape contact_form">
                  <?php echo do_shortcode('[contact-form-7 id="7" title="Tape Form"]') ?>
               </div>
               <div class="fasion contact_form">
                  <?php echo do_shortcode('[contact-form-7 id="8" title="Fasion Form"]') ?>
               </div>
            </div>
          </div>
        </div>
      </section>
    </main>
    
    <footer id="footer" class="footer">
      <?php get_template_part('template/template','footer')?>
    </footer>
  </div>

<?php get_footer(); ?>
</body>
</html>
